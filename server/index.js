var express = require('express')
var app = express()
var bodyParser = require('body-parser');
var fs = require('fs');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://10.47.54.224:27017/todoDb";
var USE_DATABASE = true;
var database;

if(USE_DATABASE){
    MongoClient.connect(url, function (err, db) {
        if (err) {
            console.log("Could not connect to database");
            throw err;
        } else {
            database = db;
            database.createCollection('todo', {
                strict: true
            }, function (err, collection) {
                if (err) {
                    console.log("todo DB already exist, you are doing something wrong");
                } else {
                    var todoItems = JSON.parse(fs.readFileSync('tasks.json', 'utf8'));
                    collection.insertMany(todoItems, function (err, result) {
                        if (err)
                            console.dir(err);
                        // else
                        //     console.dir(result);
                    });
                }
            });
        }
    
    });
}

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.get('/', function (req, res) {

    if (!USE_DATABASE) {
        var file = fs.readFileSync('tasks.json');
        var parsedFile = JSON.parse(file);
        res.send(parsedFile)
    } else {
        try {
            var collection = database.collection('todo');
            collection.find().toArray(function (err, items) {
                if (err) {
                    console.dir(err);
                    console.log("Error retrieving items");
                } else {
                    res.send(items)
                }
            });
        } catch (err) {
            console.dir(err);
        }
    }
})

app.post('/', function (req, res) {

    if (!USE_DATABASE) {
        var file = fs.readFileSync('tasks.json');
        var parsedFile = JSON.parse(file);
        parsedFile.push(req.body);
        var configJSON = JSON.stringify(parsedFile);
        fs.writeFileSync('tasks.json', configJSON);
        res.send('Hello World')
    } else {
        try {
            var collection = database.collection('todo');
            collection.insertOne(
                req.body,
                function (err, items) {
                    if (err) {
                        console.log("Error");
                    } else {
                        res.send(items);
                    }
                });
        } catch (err) {
            console.dir(err);
        }

    }
})

app.delete('/:item', function (req, res) {
    var itemNo = req.params.item;
    if (!USE_DATABASE) {

        var file = fs.readFileSync('tasks.json');
        var parsedFile = JSON.parse(file);
        parsedFile = parsedFile.filter(task => task.item != itemNo);
        var configJSON = JSON.stringify(parsedFile);
        fs.writeFileSync('tasks.json', configJSON);
        res.send(parsedFile);
    } else {
        var collection = database.collection('todo');
        collection.remove({
            item: itemNo
        }, function (err) {
            if (err) {
                console.dir(err);
            } else {
                res.send("hello");
            }
        });
    }
})

app.listen(4000);