var gulp        = require('gulp');
var browserSync = require('browser-sync').create();

// Static server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        files: ["index.html", "app.js","main.css"]
    });
});

gulp.task('default', ['browser-sync']);