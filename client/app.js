var todo = angular.module("todo", []);

todo.controller("myCtrl", function ($scope, $http) {

    $http.get("http://10.47.54.224:4000")
        .then(function (response) {
            $scope.tasks = response.data;
        });

    $scope.placeholder = 'Enter your tasks here...';

    $scope.addMe = function (add) {
        $scope.placeholder = ' Enter your tasks here...';

        if (add !== undefined) {
            if ($scope.tasks.find(x => x.item === add)) {
                $scope.placeholder = "Hurray! already exists";
            } else {
                var dataObj = {
                    item: add
                };
                $http.post("http://10.47.54.224:4000", dataObj)
                    .then(function (response) {
                        if (response.status == 200) {
                            $http.get("http://10.47.54.224:4000")
                                .then(function (response) {
                                    $scope.tasks = response.data;
                                });
                        }
                    });
            }
        } else {
            $scope.placeholder = "Hey! Enter something";
        }
        $scope.add = undefined;
    }

    $scope.delete = function (remove) {
        $scope.placeholder = ' Enter your tasks here...';
        $scope.add = undefined;
        var del = "/" + remove;
        $http.delete("http://10.47.54.224:4000" + del).then(function (response) {
            if (response.status == 200) {
                $http.get("http://10.47.54.224:4000")
                    .then(function (response) {
                        $scope.tasks = response.data;
                    });
            }
        });
    };
})